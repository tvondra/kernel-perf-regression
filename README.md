kernel perf regression (4x e5-4620)
===================================

1) simple transaction on unlogged tables
----------------------------------------

- driven by script `run-test.sh`
- executes simple transaction on scale 300 with unlogged tables

    \set aid random (1,30000000)
    \set tid random (1,3000)
    
    BEGIN;
    SELECT abalance FROM pgbench_accounts WHERE aid = :aid for UPDATE;
    SAVEPOINT s1;
    SELECT tbalance FROM pgbench_tellers WHERE tid = :tid for UPDATE;
    SAVEPOINT s2;
    SELECT abalance FROM pgbench_accounts WHERE aid = :aid for UPDATE;
    END;

- does 5 runs for 32 and 64 clients
- each run is 5 minutes (checkpoint + vacuum analyze in between)


2) regular pgbench (read-only + read-write)
-------------------------------------------

- driven by script `run-regular.sh`
- both read-only and read-write, 32 and 64 clients, on WAL-logged tables
- 5 runs, 5 minutes each


configs
-------
- kernel configuration files used (with vanilla kernels)
