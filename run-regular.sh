#!/bin/bash

VERSION=$1
DATADIR=/data/sekondquad/clog-data
RUNS=5
CLIENTS="32 64"
DURATION=300
SCALE=300

echo `date +%s` "running: $d"

export PATH=/home/sekondquad/clog-builds/pg-9.6-master/bin:$PATH

# make sure there's nothing running
killall postgres > /dev/null 2>&1

rm -Rf $DATADIR

d=kernel-$VERSION

mkdir $d

uname -a > $d/kernel.log 2>&1

which pg_config > $d/which.log 2>&1

pg_config  > $d/config.log 2>&1

pg_ctl -D $DATADIR init > $d/init.log 2>&1

cp postgresql.conf $DATADIR

pg_ctl -D $DATADIR -l $d/pg.log -w start

psql -c "select * from pg_settings" postgres > $d/settings.log 2>&1

createdb pgbench

echo `date +%s` "init"

pgbench -i -s $SCALE pgbench > $d/init-data.log 2>&1

echo `date +%s` "warmup : start"

pgbench -c 32 -j 8 -M prepared -T $DURATION -f test.sql pgbench > /dev/null 2>&1

for r in `seq 1 $RUNS`; do

	for c in $CLIENTS; do

		t=$((c/4))

		if [ $t -eq 0 ]; then
			t=1
		fi

		echo `date +%s` "ro : clients $c threads $t run $r : start"

		psql -c "checkpoint" postgres > /dev/null 2>&1
		psql -c "vacuum analyze" pgbench > /dev/null 2>&1

		pgbench -c $c -j $t -M prepared -S -T $DURATION -f test.sql pgbench > $d/ro-clients-$c-$r.log 2>&1;

		echo `date +%s` "ro : clients $c threads $t run $r : done"

	done;

done;

for r in `seq 1 $RUNS`; do

        for c in $CLIENTS; do

                t=$((c/4))

                if [ $t -eq 0 ]; then
                        t=1
                fi

                echo `date +%s` "rw : clients $c threads $t run $r : start"

                psql -c "checkpoint" postgres > /dev/null 2>&1
                psql -c "vacuum analyze" pgbench > /dev/null 2>&1

                pgbench -c $c -j $t -M prepared -T $DURATION -f test.sql pgbench > $d/rw-clients-$c-$r.log 2>&1;

                echo `date +%s` "rw : clients $c threads $t run $r : done"

        done;

done;


pg_ctl -D $DATADIR stop
