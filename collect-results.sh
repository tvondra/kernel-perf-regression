for k in kernel-3.19.8 kernel-4.1.33 kernel-4.2.8 kernel-4.3.0 kernel-4.3.6 kernel-4.4.0 kernel-4.4.23 kernel-4.4.23-2 kernel-4.5.5 kernel-4.7.6; do

	for c in 32 64; do

		for r in `seq 1 5`; do

			tps=`grep excl $k/clients-$c-$r.log | awk '{print $3}'`

			echo $k $c $r $tps

		done

	done

done


for k in kernel-3.19.8-regular kernel-4.4.23-regular; do

	for t in ro rw; do

		for c in 32 64; do

			for r in `seq 1 5`; do

				tps=`grep excl $k/$t-clients-$c-$r.log | awk '{print $3}'`

				echo $k $c $r $t $tps

			done

		done

	done

done
